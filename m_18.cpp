// m_18.cpp Based on: https://unrealistic.dev/posts/implementing-a-stack-using-a-dynamic-array-in-c
//

#include <iostream>
#include <string>

template <typename T>
class Stack
{
private:
    T* Storage;
    int Capacity;
    int Top;


public:
    Stack()
    {
        Capacity = 1;
        Storage = new T[Capacity];
        Top = -1;
    }

    ~Stack()
    {
        delete[] Storage;
    }

    void Resize(int Capacity)
    {
        T* Temp = new T[Capacity];
        for (int i = 0; i <= Top; i++)
        {
            Temp[i] = Storage[i];
        }
        delete[] Storage;
        Storage = Temp;
    }

    void Push(T Item)
    {
        if (Top < 0)
        {
            Storage[++Top] = Item;
        }
        else
        {
            Resize(++Capacity);
            Top++;
            Storage[Top] = Item;
        }
        
    }

    T Pop()
    {

        if (Top < 0) {
            throw "Exception: Array Underflow Detection!";
        }

        T temp = Storage[Top];
        Top--;
        Resize(--Capacity);
        return temp;
    }

};

int main()
{
    try
    {
        Stack<std::string> notes;
        notes.Push("hello");
        notes.Push("world");
        notes.Pop();
        notes.Push("skillbox");
        notes.Push("!");

        std::cout << notes.Pop() << "\n";
        std::cout << notes.Pop() << "\n";
        std::cout << notes.Pop() << "\n";
        //std::cout << notes.Pop() << "\n"; // throw exception

        Stack<double> doubles;
        doubles.Push(5.2);
        doubles.Push(1);
        doubles.Push(4);
        std::cout << doubles.Pop() << "\n";
        std::cout << doubles.Pop() << "\n";
        std::cout << doubles.Pop() << "\n";
    }
    catch (const char* msg) 
    {
        std::cout << msg << std::endl;
    }


}

